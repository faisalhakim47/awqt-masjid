const { build } = require('./build_system.js');

build()
  .then((result) => console.log(result.toString()))
  .catch((error) => console.warn(error));
