const child_process = require('child_process');
const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MinifyPlugin = require('babel-minify-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

function build() {
  return new Promise((resolve, reject) => {
    webpack(
      {
        mode: 'production',
        entry: {
          app: p('./app/index.js'),
          app_style: p('./app/index.css.js'),
        },
        output: {
          path: p('./public'),
          filename: '[name].[hash].js',
          chunkFilename: '[name].[hash].chunk.js',
        },
        module: {
          rules: [
            {
              test: /\.js$/,
              exclude: /node_modules/,
              use: [{ loader: 'babel-loader' }],
            },
            {
              test: /\.css$/,
              use: [MiniCssExtractPlugin.loader, { loader: 'css-loader' }],
            },
            {
              test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
              use: [
                {
                  loader: 'url-loader',
                  options: {
                    limit: 100,
                  },
                },
              ],
            },
          ],
        },
        optimization: {
          minimizer: [new OptimizeCSSAssetsPlugin({})],
        },
        plugins: [
          new MinifyPlugin(),
          new MiniCssExtractPlugin({
            filename: '[name].[hash].css',
            chunkFilename: '[id].[hash].css',
          }),
          new HtmlWebpackPlugin({
            template: p('./index.template.html'),
            filename: 'index.html',
          }),
        ],
        externals: {
          vue: 'Vue',
          'vue-router': 'VueRouter',
        },
      },
      (error, stats) => {
        link('./static', './public/static');

        link('./awqt', './public/awqt');
        link('./api', './public/api');
        link('./data', './public/data');

        link('./config.json', './public/config.json');
        link('./.htaccess.prod', './public/.htaccess');
        link('./manifest.json', './public/manifest.json');

        if (error) return reject(error);
        else resolve(stats);
      }
    );

    function link(source, to) {
      fs.unlink(p(to), () => {
        child_process.exec(`ln -s ${p(source)} ${p(to)}`);
      });
    }

    function p(relativePath) {
      return path.join(__dirname, relativePath);
    }
  });
}

module.exports = { build };
