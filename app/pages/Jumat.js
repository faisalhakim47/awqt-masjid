const Vue = require('vue').default
import { toDigit } from '../vue-tampan/index.js'
import { api } from '../tools/api.js'

const JUMAT_TIME = 'JUMAT_TIME'
const JUMAT_TIME_BASE = 'JUMAT_TIME_BASE'

export default Vue.extend({
  data() {
    return {
      configurations: [],
      JUMAT_TIME: new Date(0, 0, 0, 11, 50),
      JUMAT_TIME_BASE: 'dzuhur',
      jumatTimeBaseOptions: [
        { value: 'dzuhur', label: 'Mengukuti waktu dzuhur' },
        { value: 'manual', label: 'Tentukan sendiri' },
      ],
    }
  },

  methods: {
    api,

    loadConfiguratios() {
      const loading = api('/awqt/api/get.configuration-list.php')
        .then((configurations) => {
          configurations.forEach(({ name, value }) => {
            switch (name) {
              case JUMAT_TIME:
                const [hours, minutes] = value
                  .split(':')
                  .map((t) => parseInt(t, 10))
                this.JUMAT_TIME = new Date(0, 0, 0, hours, minutes)
                break
              case JUMAT_TIME_BASE:
                this.JUMAT_TIME_BASE = value
                break
              case 'TIME_ZONE':
                this.timeZone = value.split(':')
                this.province = this.timeZone[0]
                this.city = this.timeZone[1]
            }
          })
          if (this.timeZone === undefined) {
            return api('/awqt/api/patch.configuration-item.php', {
              data: {
                TIME_ZONE: 'JAWA TENGAH:KOTA SEMARANG',
              },
            }).then(this.loadConfiguratios)
          }
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal memuat konfigurasi',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },

    saveJumatBase() {
      const hours = toDigit(this.JUMAT_TIME.getHours(), 2)
      const minutes = toDigit(this.JUMAT_TIME.getMinutes(), 2)
      const data = {
        JUMAT_TIME: `${hours}:${minutes}`,
        JUMAT_TIME_BASE: this.JUMAT_TIME_BASE,
      }

      const jumatSchedules = api('/awqt/api/get.schedule-list.php', {
        query: { keyword: '::%::6:2:' },
      })
      const replaceScheduleIncludeTimes = (schedule_id, time_id) =>
        api('/awqt/api/get.schedule_include_time-list-by-schedule.php', {
          query: { schedule_id },
        })
          .then((include_times) => {
            const deleting = include_times.map((include_time) => {
              return api('/awqt/api/delete.schedule_include_time-item.php', {
                data: include_time,
              })
            })
            return Promise.all(deleting)
          })
          .then(() => {
            return api('/awqt/api/post.schedule_include_time-item.php', {
              data: { schedule_id, time_id },
            })
          })

      const baseSet =
        this.JUMAT_TIME_BASE === 'manual'
          ? api('/awqt/api/get.time-item.php', { query: { name: JUMAT_TIME } })
              .then((time) => {
                if (time) {
                  return api('/awqt/api/get.cron-list-by-time.php', {
                    query: { time_id: time.id },
                  })
                    .then(([cron]) => {
                      cron = {
                        ...cron,
                        hours: hours,
                        minutes: minutes,
                      }
                      return api('/awqt/api/patch.cron-item.php', {
                        data: cron,
                      })
                    })
                    .then(() => time)
                } else {
                  const time = {
                    name: JUMAT_TIME,
                  }
                  const cron = {
                    months: '*',
                    dates: '*',
                    days: '6',
                    hours: hours,
                    minutes: minutes,
                    seconds: '0',
                  }
                  return Promise.all([
                    api('/awqt/api/post.time-item.php', { data: time }),
                    api('/awqt/api/post.cron-item.php', { data: cron }),
                  ]).then(([time, cron]) => {
                    const time_cron = {
                      time_id: time.id,
                      cron_id: cron.id,
                    }
                    return api('/awqt/api/post.time_cron-item.php', {
                      data: time_cron,
                    }).then(() => time)
                  })
                }
              })
              .then((time) => {
                return jumatSchedules.then((jumatSchedules) => {
                  const replacing = jumatSchedules.map((schedule) => {
                    return replaceScheduleIncludeTimes(schedule.id, time.id)
                  })
                  return Promise.all(replacing)
                })
              })
          : jumatSchedules.then((jumatSchedules) => {
              const replacing = jumatSchedules.map((schedule) => {
                return replaceScheduleIncludeTimes(schedule.id, 2 /* DZUHUR */)
              })
              return Promise.all(replacing)
            })

      const saving = baseSet
        .then(() => api('/awqt/api/patch.configuration-item.php', { data }))
        .then(() => api('/awqt/api/post.compute_next.php'))
        .catch((error) => {
          this.$tampan.alert({
            title: "Gagal menyimpan basis hari Jum'at",
            text: error,
          })
          console.warn(error)
        })
        .then(this.loadConfiguratios)
      this.$tampan.useLoadingState(saving)
    },
  },

  mounted() {
    this.loadConfiguratios()
  },

  template: `
  <div class="page">
    <header class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title">Pengaturan Hari Jum'at</h2>
    </header>
    <div class="page-content padding">
      <row>
        <column :width="{ sm: 1, md: 1/2 }">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Waktu Jum'at</h3>
            </div>
            <div class="box-content">
              <field label="Basis waktu">
                <input-select
                  :options="jumatTimeBaseOptions"
                  v-model="JUMAT_TIME_BASE"
                  @input="saveJumatBase"
                ></input-select>
              </field>
              <field label="Waktu Jum'at" info="waktu materi audio berakhir.">
                <input-time
                  :disabled="JUMAT_TIME_BASE !== 'manual'"
                  v-model="JUMAT_TIME"
                  @input="saveJumatBase"
                ></input-time>
              </field>
            </div>
          </div>
        </column>
      </row>
    </div>
  </div>
  `,
})
