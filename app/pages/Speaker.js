const Vue = require('vue').default;
import { api } from '../tools/api.js';

export default Vue.extend({
  data() {
    return {
      volume: 0,
    };
  },

  methods: {
    api,

    fetchVolume() {
      const fetching = api('/awqt/api/get.volume.php')
        .then((result) => {
          this.volume = result.volume;
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal menghidupkan speaker',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(fetching);
      return fetching;
    },

    speakerOn() {
      const updating = api(
        '/awqt/api/post.execute_task.php?name=SpeakerOn'
      ).catch((error) => {
        this.$tampan.alert({
          title: 'Gagal menghidupkan speaker',
          text: error,
        });
        console.warn(error);
      });
      this.$tampan.useLoadingState(updating);
      return updating;
    },

    speakerOff() {
      const updating = api(
        '/awqt/api/post.execute_task.php?name=SpeakerOff'
      ).catch((error) => {
        this.$tampan.alert({
          title: 'Gagal mematikan speaker',
          text: error,
        });
        console.warn(error);
      });
      this.$tampan.useLoadingState(updating);
      return updating;
    },
  },

  mounted() {
    this.fetchVolume();
  },

  template: `
  <div class="page">
    <header class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title">Pengaturan Speaker</h2>
    </header>
    <div class="page-content padding">

      <row>
        <column :width="{ sm: 1, md: 1/2 }">
          <div class="box">
            <header class="box-header">
              <h3 class="box-title">Volume</h3>
            </header>
            <div class="box-content">
              <div class="button-group">
                <input-range
                  prefix-icon-text="volume_down"
                  sufix-icon-text="volume_up"
                  v-model="volume"
                  :max="100"
                  :min="0"
                  @input="(value) => api('/awqt/api/post.volume.php?vol=' + value)"
                ></input-range>
              </div>
            </div>
          </div>
        </column>
      </row>

      <row>
        <column :width="{ sm: 1, md: 1/2 }">
          <div class="box">
            <header class="box-header">
              <h3 class="box-title">Power Speaker</h3>
            </header>
            <div class="box-content">
              <div class="button-group">
                <button-tampan
                  color="outline"
                  icon-text="volume_up"
                  @click="speakerOn"
                >Hidupkan</button-tampan>
                <button-tampan
                  color="outline"
                  icon-text="volume_off"
                  @click="speakerOff"
                >Matikan</button-tampan>
              </div>
            </div>
          </div>
        </column>
      </row>

    </div>
  </div>
  `,
});
