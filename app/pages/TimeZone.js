const Vue = require('vue').default
import { api } from '../tools/api.js'

const JUMAT_TIME = 'JUMAT_TIME'
const JUMAT_TIME_BASE = 'JUMAT_TIME_BASE'

export default Vue.extend({
  data() {
    return {
      isChangingTimeZone: false,
      configurations: [],
      provinceOptions: [],
      province: '',
      cityOptions: [],
      city: '',
      timeZone: [],
    }
  },

  methods: {
    api,

    loadConfiguratios() {
      const loading = api('/awqt/api/get.configuration-list.php')
        .then((configurations) => {
          configurations.forEach(({ name, value }) => {
            switch (name) {
              case JUMAT_TIME:
                const [hours, minutes] = value.split(':').map((t) => parseInt(t, 10))
                this.JUMAT_TIME = new Date(0, 0, 0, hours, minutes)
                break
              case JUMAT_TIME_BASE:
                this.JUMAT_TIME_BASE = value
                break
              case 'TIME_ZONE':
                this.timeZone = value.split(':')
                this.province = this.timeZone[0]
                this.city = this.timeZone[1]
            }
          })
          if (this.timeZone === undefined) {
            return api('/awqt/api/patch.configuration-item.php', {
              data: {
                TIME_ZONE: 'JAWA TENGAH:KOTA SEMARANG',
              }
            }).then(this.loadConfiguratios)
          }
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal memuat konfigurasi',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },

    loadProvince() {
      const loading = api('/api/get.province-list.php')
        .then((provinces) => {
          this.provinceOptions = provinces
            .map((province) => {
              return {
                label: province,
                value: province,
              }
            })
            .sort((a, b) => {
              const valueA = a.value.toLowerCase()
              const valueB = b.value.toLowerCase()
              if (valueA < valueB) return -1
              if (valueA > valueB) return 1
              return 0
            })
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal memuat daftar provinsi.',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },

    loadCity() {
      const query = {
        province: this.province
      }
      const loading = api('/api/get.city-list-by-province.php', { query })
        .then((cities) => {
          this.cityOptions = cities
            .map((city) => {
              return {
                label: city,
                value: city,
              }
            })
            .sort((a, b) => {
              const valueA = a.value.toLowerCase()
              const valueB = b.value.toLowerCase()
              if (valueA < valueB) return -1
              if (valueA > valueB) return 1
              return 0
            })
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal memuat daftar kota.',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },

    changeTimezone() {
      const newTimezone = `${this.province}:${this.city}`

      if (this.isChangingTimeZone) return
      if (newTimezone === this.timeZone.join(':')) return

      this.isChangingTimeZone = true
      const changin = Promise.all([
        api('/awqt/api/patch.configuration-item.php', {
          data: {
            TIME_ZONE: newTimezone,
          },
        }),
        api('/api/patch.timezone.php', {
          query: {
            province: this.province,
            city: this.city
          },
        }),
      ]).then(this.loadConfiguratios)
        .then(() => api('/awqt/api/post.compute_next.php'))
        .then(() => this.isChangingTimeZone = false)
      this.$tampan.useLoadingState(changin)
    },
  },

  watch: {
    province() {
      this.loadCity()
    },
    city() {
      this.changeTimezone()
    },
  },

  mounted() {
    this.loadConfiguratios()
    this.loadProvince()
  },

  template: `
  <div class="page">
    <div class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title"> Jadwal Waktu Sholat</h2>
    </div>

    <div class="page-content padding">
      <row>
        <column :width="{ sm: 1, md: 1/2 }">
          <div class="box">
            <header class="box-header">
              <h3 class="box-title">Wilayah</h3>
            </header>
            <div class="box-content">
              <field label="Provinsi">
                <input-select
                  :options="provinceOptions"
                  v-model="province"
                  @input="loadCity"
                ></input-select>
              </field>
              <field label="Kota">
                <input-select
                  :options="cityOptions"
                  v-model="city"
                ></input-select>
              </field>
            </div>
          </div>
        </column>
      </row>

    </div>
  </div>
  `
})
