const Vue = require('vue').default;
import { api } from '../tools/api.js';

import CreateSchedulePack from '../components/Modal_CreateSchedulePack.js';
import SchedulePackPageContent from '../components/PageContent_SchedulePack.js';

export default Vue.extend({
  name: 'SchedulePackList',

  components: {
    CreateSchedulePack,
    SchedulePackPageContent,
  },

  data() {
    return {
      openSchedulePackSelector: false,
      openCreateSchedulePack: false,
      schedulePacks: [],
      activeSchedulePack: null,
      isLoading: true,
    };
  },

  computed: {
    isScheduleExist() {
      if (this.isLoading) return true;
      if (this.schedulePacks.length) return true;
      return false;
    },
  },

  methods: {
    loadSchedulePacks() {
      this.isLoading = true;

      const loading = api('/awqt/api/get.tag-list.php', 'tag-list', {
        query: { keyword: ':%:' },
      })
        .then((schedulePacs) => {
          return schedulePacs.map((schedulePack) => {
            return {
              ...schedulePack,
              name: schedulePack.name.slice(1, -1),
            };
          });
        })
        .then((schedulePacks) => {
          this.schedulePacks = schedulePacks;
          this.activeSchedulePack = schedulePacks.find(
            (pack) => pack.is_active
          );
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal',
            text: JSON.stringify(error),
          });
        })
        .then(() => (this.isLoading = false));

      return this.$tampan.useLoadingState(loading);
    },

    activateSchedulePack(name) {
      const activating = Promise.all(
        this.schedulePacks
          .map((pack) => `:${pack.name}:`)
          .map((tagName) => {
            return api('/awqt/api/patch.tag-item.php', {
              data: { name: tagName, is_active: 0 },
            });
          })
      )
        .then(() => {
          const tagName = `:${name}:`;
          return api('/awqt/api/patch.tag-item.php', {
            data: { name: tagName, is_active: 1 },
          });
        })
        .then(() => {
          this.schedulePacks = this.schedulePacks.map((pack) => {
            return {
              ...pack,
              is_active: false,
            };
          });
          const pack = this.schedulePacks.find((pack) => pack.name === name);
          if (pack) {
            pack.is_active = true;
            this.viewPack(pack);
          }
        })
        .then(() => api('/awqt/api/post.compute_next.php'))
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Mengaktifkan Paket',
            text: error,
          });
        });

      this.$tampan.useLoadingState(activating);
    },

    schedulePackCreated(schedulePack) {
      this.openCreateSchedulePack = false;
      this.loadSchedulePacks();
    },

    viewPack(schedulePack) {
      this.openSchedulePackSelector = false;
      this.activeSchedulePack = schedulePack;
    },
  },

  mounted() {
    this.loadSchedulePacks();
  },

  template: `
  <div id="schedule-pack-list" class="page">
    <div class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2
        class="page-title clickable"
        style="cursor: pointer; display: flex; align-items: center;"
        @click="openSchedulePackSelector = true"
      >
        Paket: {{ activeSchedulePack ? activeSchedulePack.name : '...' }}
        <span
          class="icon material-icons"
          style="margin: .25rem; font-size: 2rem; color: #FFF;"
        >arrow_drop_down</span>
      </h2>
    </div>

    <schedule-pack-page-content
      v-if="activeSchedulePack"
      :name="activeSchedulePack.name"
    ></schedule-pack-page-content>

    <modal :show="openSchedulePackSelector" @close="openSchedulePackSelector = false">
      <header slot="header" class="modal-header">
        <h3 slot="header" class="modal-title">Pilih Jadwal</h3>
      </header>
      <table class="table borderless" :class="{ hoverable: !$tampan.client.isMobileOS }">
        <tbody style="user-select: none;">
          <tr v-for="pack in schedulePacks">
            <td>{{ pack.name }}</td>
            <td style="text-align: right;">
              <div class="button-group align-right">
                <button-tampan
                  :display="pack.is_active ? '' : 'positive'"
                  :disabled="!!pack.is_active"
                  :icon-text="pack.is_active ? 'check' : ''"
                  @click="activateSchedulePack(pack.name)"
                >{{ pack.is_active ? '' : 'Aktifkan'}}</button-tampan>
                <button-tampan
                  @click="viewPack(pack)"
                >Lihat</button-tampan>
              </div>
            </td>
          </tr>
          <tr v-if="!schedulePacks.length">
            <td class="emptyinfo" colspan="2">
              Belum ada paket jadwal. Silahkan buat paket jadwal terlebih dahulu.
            </td>
          </tr>
        </tbody>
      </table>
      <footer slot="footer" class="modal-footer">
        <div class="button-group justify-between">
          <button-tampan
            icon-text="close"
            @click="openSchedulePackSelector = false"
          >Tutup</button-tampan>
          <button-tampan
            display="positive outline"
            icon-text="add"
            @click="openCreateSchedulePack = true"
          >Buat Paket Jadwal</button-tampan>
        </div>
      </footer>
    </modal>

    <create-schedule-pack
      :show="!isScheduleExist || openCreateSchedulePack"
      :cancelable="isScheduleExist"
      @created="schedulePackCreated"
      @close="openCreateSchedulePack = false"
    ></create-schedule-pack>
  </div>
  `,
});
