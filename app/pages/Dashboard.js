const Vue = require('vue').default;
import { toDigit, toTimeHour } from '../vue-tampan/index.js';
import { api } from '../tools/api.js';
import { durationFormat } from '../model/audio.js';

export default Vue.extend({
  name: 'Dashboard',

  data() {
    return {
      activeTasks: [],
      audios: [],
      times: [],
      time_left: 0,
      timezone: new Date().getTimezoneOffset() / 60,
      timeZoneString: '',
      serverTime: 0,
    };
  },

  computed: {
    time() {
      if (this.time_left < 0) return '00:00:00';
      const date = new Date(this.time_left);
      date.setHours(date.getHours() + this.timezone);
      return `${toDigit(date.getHours(), 2)}:${toDigit(
        date.getMinutes(),
        2
      )}:${toDigit(date.getSeconds(), 2)}`;
    },
    serverTimeString() {
      const date = new Date(this.serverTime);
      return `${toDigit(date.getHours(), 2)}:${toDigit(
        date.getMinutes(),
        2
      )}:${toDigit(date.getSeconds(), 2)}`;
    },
    nextTime() {
      return this.times.find((time) => {
        const date = new Date();
        date.setHours(parseInt(time.hours, 10));
        date.setMinutes(parseInt(time.minutes, 10));
        return date.getTime() > new Date().getTime();
      });
    },
    totalAudios() {
      return durationFormat(
        this.audios
          .map((audio) => {
            return audio.duration;
          })
          .reduce((sum, duration) => {
            return sum + duration;
          }, 0)
      );
    },
    prayerTimes() {
      let isNextTimeFound = false;
      const now = new Date();
      if (!Array.isArray(this.times)) {
        return [];
      }
      return this.times.map((time) => {
        const prayerDate = new Date(
          now.getFullYear(),
          parseInt(time.months, 10) - 1,
          parseInt(time.dates, 10),
          parseInt(time.hours, 10),
          parseInt(time.minutes, 10)
        );
        const prayerTime = prayerDate.getTime();
        const audioTask = (this.activeTasks || []).find((activeTask) => {
          return activeTask.time_id === time.time_id;
        });
        const beginTime = audioTask
          ? `${toTimeHour(new Date(prayerTime - audioTask.duration))} - `
          : '';
        const isNextTime = isNextTimeFound ? false : prayerTime > now.getTime();
        if (isNextTime) isNextTimeFound = true;
        return {
          isNextTime,
          timeName: time.time_name,
          timeDiff: `${beginTime}${toTimeHour(prayerDate)}`,
        };
      });
    },

    provinceZone() {
      const province = this.timeZoneString.split(':')[0];
      return (province || '').toLowerCase();
    },

    cityZone() {
      const city = this.timeZoneString.split(':')[1];
      return (city || '').toLowerCase();
    },
  },

  methods: {
    durationFormat,
    toDigit,

    fetchActiveTasks() {
      const fetching = api('/awqt/api/get.active_task-list.php')
        .then((data) => {
          this.activeTasks = data.result;
          const nowTime = new Date().getTime();
          const audioPlayTask = this.activeTasks.find(({ task_name, time }) => {
            return (
              task_name === 'AudioPlayUsingSpeaker' &&
              new Date(time).getTime() > nowTime
            );
          });
          if (audioPlayTask) {
            this.time_left =
              new Date(audioPlayTask.time).getTime() -
              audioPlayTask.duration -
              new Date().getTime();
            const { md5s } = JSON.parse(audioPlayTask.payload);
            Promise.all(
              md5s.map((md5) =>
                api(`/awqt/api/get.audio-item.php`, { query: { md5 } })
              )
            ).then((audios) => {
              this.audios = audios;
            });
          }
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memulat Timer',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(fetching);
    },

    fetchServerTime() {
      const fetching = api('/awqt/api/get.server_time.php')
        .then((server_time) => {
          this.serverTime = new Date(server_time.time).getTime();
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memulat Waktu Server',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(fetching);
    },

    fetchTimes() {
      const now = new Date();
      const query = {
        months: now.getMonth() + 1,
        dates: now.getDate(),
      };
      const fetching = api('/api/get.time-list-by-date.php', { query })
        .then((times) => {
          this.times = times;
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memulat Waktu Sholat',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(fetching);
    },

    fetchTimeZone() {
      const fetching = api('/awqt/api/get.configuration-list.php').then(
        (configurations) => {
          const timeZoneString = configurations.find((configuration) => {
            return configuration.name === 'TIME_ZONE';
          });
          if (timeZoneString) this.timeZoneString = timeZoneString.value;
        }
      );
      return this.$tampan.useLoadingState(fetching);
    },
  },

  mounted() {
    this.fetchActiveTasks();
    this.fetchServerTime();
    this.fetchTimes();
    this.fetchTimeZone();
    this.animatingTime = true;
    let prevTime = performance.now();
    const animateTime = (time) => {
      const diff = time - prevTime;
      this.time_left -= diff;
      this.serverTime += diff;
      prevTime = time;
      if (this.animatingTime) requestAnimationFrame(animateTime);
    };
    requestAnimationFrame(animateTime);
  },

  beforeDestroy() {
    this.animatingTime = false;
  },

  template: `
  <div id="dashboard" class="page">
    <div class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title">Dashboard</h2>
    </div>
    <div class="page-content padding">
      <p style="
        margin: 2rem;
        text-align: center;
      ">
        <img style="width: 200px;" src="/static/images/mosque.svg">
      </p>
      <p style="
        font-size: 1.6rem;
        font-weight: 400;
        text-align: center;
      ">Jadwal Selanjutnya</p>
      <p style="
        margin: 0rem;
        text-align: center;
        font-size: 4rem;
        font-weight: 300;
        font-variant-numeric: tabular-nums;
      ">
        {{ time }}
      </p>
      <p style="
        margin: 0 auto 2rem auto;
        text-align: center;
        font-size: 1.5rem;
        font-weight: 300;
        font-variant-numeric: tabular-nums;
      ">waktu saat ini {{ serverTimeString }}<br>jadwal wilayah <router-link :to="{ name: 'TimeZone' }" style="text-transform: capitalize">{{ provinceZone }} {{ cityZone }}</router-link>
      </p>
      <section class="box">
        <div class="box-content">
          <table class="table borderless">
            <thead>
              <tr>
                <th colspan="2">Jadwal Program Hari Ini</th>
              </tr>
            </thead>
            <tbody>
              <tr
                v-for="time in prayerTimes"
                :key="time.timeName"
                :style="time.isNextTime ? 'font-weight: 500;' : ''"
              >
                <td>{{ time.timeName }}</td>
                <td style="font-variant-numeric: tabular-nums;">{{ time.timeDiff }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </section>
      <section class="box">
        <div class="box-content">
          <table class="table borderless">
            <thead>
              <tr>
                <th>Materi Selanjutnya</th>
                <th>Durasi</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="audio in audios" :key="audio.md5">
                <td>{{ audio.filename }}</td>
                <td style="font-variant-numeric: tabular-nums;">{{ durationFormat(audio.duration) }}</td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td style="text-align: right;">total</td>
                <td style="font-weight: 500; font-variant-numeric: tabular-nums;">{{ totalAudios }}</td>
              </tr>
            </tfoot>
          </table>
        </div>
      </section>
      <div class="button-group align-center">
        <button-tampan
          display="positive"
          icon-text="settings"
          @click="$router.push({ name: 'SchedulePackList' })"
        >Edit Paket Jadwal</button-tampan>
      </div>
    </div>
  </div>
  `,
});
