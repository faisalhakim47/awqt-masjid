const Vue = require('vue').default

export default Vue.extend({
  template: `
  <div id="tutorial" class="page">
    <header class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title">Panduan Alat dan Aplikasi Awqot</h2>
    </header>
    <div class="page-content" style="overflow: hidden;">
      <iframe src="/static/tutorial"></iframe>
    </div>
  </div>
  `
})
