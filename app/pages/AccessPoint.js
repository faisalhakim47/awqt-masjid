const Vue = require('vue').default
import { api } from '../tools/api.js'

export default Vue.extend({
  data() {
    return {
      wifi: {
        ssid: '',
        passphrase: '',
      },
    }
  },

  methods: {
    fetchWifi() {
      const fetching = api('/api/get.wifi.php')
        .then((wifi) => {
          this.wifi = wifi
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal memuat wifi password.',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(fetching)
    },

    saveWifi() {
      this.$tampan.confirm({
        title: 'PERHATIAN!',
        text: 'Setelah menyimpan, silahkan matikan dan hidupkan lagi wifi pada perangkat anda. Kemudian masuk kembali menggunakan wifi dan password baru.',
        confirmText: 'Simpan',
      }).then(() => {
        const patching = api('/awqt/api/patch.wifi.php', { data: this.wifi })
          .catch((error) => {
            this.$tampan.alert({
              title: 'Gagal menyimpan wifi password.',
              text: error,
            })
            console.warn(error)
          })
        this.$tampan.useLoadingState(patching)
      })
    },
  },

  mounted() {
    this.fetchWifi()
  },

  template: `
  <div class="page">
    <header class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title">Pengaturan Wifi</h2>
    </header>
    <div class="page-content padding">

      <row>
        <column :width="{ sm: 1, md: 1/2 }">
          <div class="box">
            <header class="box-header">
              <h3 class="box-title">Nama dan Password Wifi</h3>
            </header>
            <div class="box-content">
              <field label="SSID (Nama Wifi)">
                <input type="text" v-model="wifi.ssid">
              </field>
              <field label="Password" info="PENTING! simpan password wifi ini ditempan yang aman.">
                <input type="text" v-model="wifi.passphrase">
              </field>
              <div class="button-group align-right">
                <button-tampan
                  color="positive"
                  icon-text="save"
                  @click="saveWifi"
                >Simpan</button-tampan>
              </div>
            </div>
          </div>
        </column>
      </row>

    </div>
  </div>
  `
})
