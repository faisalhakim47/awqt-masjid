const Vue = require('vue').default;
import { api } from '../tools/api.js';
import { durationFormat } from '../model/audio.js';

import AudioUpload from '../components/Modal_AudioUpload.js';
import CreatePlaylist from '../components/Modal_CreatePlaylist.js';
import PlayerControls from '../components/PlayerControls.js';

export default Vue.extend({
  components: {
    AudioUpload,
    CreatePlaylist,
    PlayerControls,
  },

  data() {
    return {
      openCreatePlaylist: false,
      openAudioUpload: false,
      activePlaylist: null,
      playlist_id: 0,
      playlists: [],
      md5: '',
      md5s: [],
      tab: 'audios',
      tabs: [
        { value: 'audios', label: 'Semua Materi' },
        { value: 'playlist', label: 'Playlist' },
      ],
      keyword: '',
    };
  },

  computed: {
    numberOfColumn() {
      if (this.$tampan.client.isSmallScreen) return 1;
      else if (this.$tampan.client.isMediumScreen) return 2;
      else return 3;
    },
  },

  methods: {
    durationFormat,

    fetchAudioMd5s() {
      const fetching = api('/api/get.audio_md5-list.php')
        .then((md5s) => {
          this.md5 = md5s[0];
          this.md5s = md5s;
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memulat Audio',
            text: JSON.stringify(error),
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(fetching);
    },

    fetchPlaylist() {
      const fetching = api('/awqt/api/get.playlist-list.php')
        .then((playlists) => {
          this.playlists = playlists;
          const activePlaylist = playlists[0];
          if (activePlaylist) this.activePlaylist = activePlaylist;
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memulat Playlist',
            text: JSON.stringify(error),
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(fetching);
    },
  },

  mounted() {
    this.fetchAudioMd5s();
    this.fetchPlaylist();
  },

  template: `
  <div id="mediaplayer" class="page">
    <div class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title">Materi Player</h2>
    </div>

    <div
      class="page-content"
      :class="tab === 'playlist' && 'padding'"
      :style="tab === 'audios' && 'overflow: hidden;'"
    >
      <tabs :options="tabs" v-model="tab"></tabs>

      <player-controls
        v-show="tab === 'audios'"
        :md5="md5"
        :md5s="md5s"
      ></player-controls>

      <list-block
        v-show="tab === 'audios'"
        src="/awqt/api/get.audio-list.php"
        :query="'filename=%'+keyword+'%'"
        :number-of-column="numberOfColumn"
        :row-height="$tampan.client.isSmallScreen ? 72 : 80"
        :gap="4"
      >
        <div
          slot="content"
          slot-scope="{ data }"
          class="list-block-content"
          :class="data.md5 === $tampan.md5 && 'active'"
        >
          <div class="list-block-detail" @click="$tampan.playAudio(data.md5, md5s)">
            <div class="list-block-label">
              <span
                v-show="data.md5 === $tampan.md5"
                class="icon material-icons"
              >{{ $tampan.isPlaying ? 'play_arrow' : 'pause' }}</span>
              {{ data.filename }}
            </div>
            <div class="list-block-description">{{ durationFormat(data.duration) }}</div>
          </div>
          <!-- <button-tampan
            icon-text="info"
            @click="$tampan.playAudio(data.md5, md5s)"
          ></button-tampan> -->
        </div>
        <p
          slot="content-empty"
          style="
            font-style: italic;
            text-align: center;
            color: #616161;
            margin: 2rem 0;
          "
        >Materi tidak ditemukan.</p>
      </list-block>

      <button-action
        v-show="tab === 'audios'"
        icon-text="library_add"
        @click="openAudioUpload = true"
      ></button-action>

      <audio-upload
        :show="openAudioUpload"
        :upload-only="true"
        @close="openAudioUpload = false"
      ></audio-upload>

      <row v-show="tab === 'playlist'">
        <column :width="{ lg: 1/2 }">
          <div class="box">
            <div class="box-content">
              <table v-if="playlists.length > 0" class="table borderless clickable">
                <tbody>
                  <router-link
                    tag="tr"
                    v-for="playlist in playlists"
                    :key="playlist.id"
                    :to="{ name: 'PlaylistItem', params: { id: playlist.id } }"
                  >
                    <td>{{ playlist.name }}</td>
                  </router-link>
                </tbody>
              </table>
              <p v-else>balum ada playlist, silahkan buat playlist terlebih dahulu.</p>
            </div>
          </div>
        </column>
      </row>
    </div>

    <button-action
      v-if="tab === 'playlist'"
      icon-text="playlist_add"
      @click="openCreatePlaylist = true"
    ></button-action>

    <create-playlist
      :show="openCreatePlaylist"
      @close="openCreatePlaylist = false"
      @created="fetchPlaylist"
    ></create-playlist>
  </div>
  `,
});
