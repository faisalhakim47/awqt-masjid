const Vue = require('vue').default;
import { api } from '../tools/api.js';

export default Vue.extend({
  data() {
    return {
      isWaiting: false,
      status: '',
    };
  },

  methods: {
    wait() {
      this.redirect = setTimeout(() => {});
      if (!this.isWaiting) return;
      setTimeout(() => {
        if (!this.isWaiting) return;
        api('/api/get.is_awqt_app.php').then(() => {
          clearTimeout(this.redirect);
          this.redirect = setTimeout(() => (window.location = '/'), 5000);
        });
        this.wait();
      }, 3000);
    },

    upgrade() {
      this.$tampan
        .confirm({
          title: 'PERHATIAN',
          text:
            'Update software memerlukan waktu kurang lebih 5 menit. Selama waktu tersebut aplikasi awqot tidak akan dapat digunakan.',
          confirmText: 'Update Sekarang',
        })
        .then(() => {
          this.isWaiting = true;
          this.status = 'Menyiapkan pembaruan ... (0/5)';
          const upgrading = api('/api/post.upgrader.php?command=stop_apps')
            .then(() => {
              this.status = 'Mengunduh pembaruan ... (1/5)';
              return api('/api/post.upgrader.php?command=update_repositories');
            })
            .then(() => {
              this.status = 'Memperbarui basis data ... (2/5)';
              return api('/api/post.upgrader.php?command=update_database');
            })
            .then(() => {
              this.status = 'Memuat ulang aplikasi ... (3/5)';
              return api('/api/post.upgrader.php?command=start_apps');
            })
            .then(() => {
              return api('/api/post.upgrader.php?command=restart_updater');
            })
            .then(() => new Promise((resolve) => setTimeout(resolve, 15000)))
            .then(() => {
              this.status = 'Memperbarui aplikasi ... (4/5)';
              return api('/api/post.upgrader.php?command=build');
            })
            .catch((error) => {
              this.isWaiting = false;
              console.warn(error);
              return this.$tampan.alert({
                title: 'Gagal Memperbarui Software.',
                text: error,
              });
            })
            .then(() => {
              setTimeout(() => {
                this.status = 'Selesai ... (5/5)';
              }, 3000);
              setTimeout(() => {
                window.location = '/';
                this.isWaiting = false;
              }, 5000);
            });
          this.$tampan.useLoadingState(upgrading);
        });
    },
    reboot() {
      this.$tampan
        .confirm({
          title: 'PERHATIAN',
          text:
            'Restart software memerlukan waktu kurang lebih 5 menit. Selama waktu tersebut aplikasi awqot tidak akan dapat digunakan.',
          confirmText: 'Restart Sekarang',
        })
        .then(() => {
          this.isWaiting = true;
          api('/api/post.upgrader.php?command=reboot');
          this.wait();
        });
    },
  },

  template: `
  <div class="page" id="upgrade">
    <header class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title">Software</h2>
    </header>
    <div class="page-content padding">
      <div style="height: 100%; display: flex; justify-content: center; align-items: center;">
        <button-tampan color="large" icon-text="system_update" @click="upgrade">Update</button-tampan>
        <button-tampan color="large" icon-text="replay" @click="reboot">Restart</button-tampan>
      </div>
    </div>
    <div v-if="isWaiting" class="upgrading_overlay">
      <p>{{ status }}</p>
    </div>
  </div>
  `,
});
