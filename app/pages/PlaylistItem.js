const Vue = require('vue').default;
import { api } from '../tools/api.js';
import { durationFormat } from '../model/audio.js';

import AudioUpload from '../components/Modal_AudioUpload.js';
import PlayerControls from '../components/PlayerControls.js';
import EditPlaylist from '../components/Modal_EditPlaylist.js';

export default Vue.extend({
  props: {
    id: { type: [Number, String], required: true },
  },

  components: {
    AudioUpload,
    PlayerControls,
    EditPlaylist,
  },

  data() {
    return {
      openEditPlaylist: false,
      openAudioUpload: false,
      isPlaying: false,
      playlist: {
        id: 0,
        name: '-',
        audios: [],
      },
      keyword: '',
      selectedAudioIndex: -1,
    };
  },

  computed: {
    numericId() {
      return parseInt(this.id, 10) || 0;
    },

    isAudioSelected() {
      return (
        typeof this.selectedAudioIndex === 'number' &&
        this.selectedAudioIndex > -1
      );
    },

    md5s() {
      return this.playlist.audios.map((audio) => {
        return audio.md5;
      });
    },

    md5() {
      return this.md5s[0] || '';
    },

    isPlaylist() {
      return !!this.playlistId;
    },

    numberOfColumn() {
      if (this.$tampan.client.isSmallScreen) return 1;
      else if (this.$tampan.client.isMediumScreen) return 2;
      else return 3;
    },
  },

  methods: {
    durationFormat,

    fetchMd5s() {
      const fetching = api('/api/get.playlist_audio-list.php?id=' + this.id)
        .then((playlist) => {
          this.playlist = playlist;
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memuat Daftar Audio',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(fetching);
    },

    onAudioUploadDone(audios) {
      this.playlist.audios = [...this.playlist.audios, ...audios];
      return this.savePlaylist();
    },

    onPlaylistSaved() {
      this.fetchMd5s();
    },

    onPlaylistDeleted() {
      this.$router.push({ name: 'MediaPlayer' });
    },

    savePlaylist() {
      const posting = api('/awqt/api/patch.playlist-item.php', {
        data: {
          id: this.id,
          name: this.playlist.name,
          audio_md5s: JSON.stringify(
            this.playlist.audios.map((audio) => {
              return audio.md5;
            })
          ),
        },
      }).then(this.fetchMd5s);
      return this.$tampan.useLoadingState(posting);
    },

    changeOrder(offset) {
      if (this.isOrdering) return;
      this.isOrdering = true;
      const audios = [...this.playlist.audios];
      const targetIndex = this.selectedAudioIndex + offset;
      if (targetIndex < 0 || targetIndex === this.playlist.audios.length) {
        this.isOrdering = false;
        return Promise.resolve();
      }
      const movedAudio = audios.splice(this.selectedAudioIndex, 1);
      audios.splice(targetIndex, 0, ...movedAudio);
      this.selectedAudioIndex = targetIndex;
      this.playlist.audios = audios;
      return this.savePlaylist().then(() => {
        this.isOrdering = false;
      });
    },

    deleteAudio() {
      if (this.isDeleting) return;
      this.$tampan
        .confirm({
          title: 'Anda yakin menghapus audio ini?',
          confirmText: 'Hapus',
          confirmIconText: 'delete_forever',
        })
        .then(() => {
          this.isDeleting = true;
          const audios = this.playlist.audios;
          audios.splice(audios[this.selectedAudioIndex], 1);
          this.selectedAudioIndex = -1;
          return this.savePlaylist().then(() => {
            this.isDeleting = false;
          });
        });
    },
  },

  watch: {
    id() {
      this.fetchMd5s();
    },
  },

  mounted() {
    this.fetchMd5s();
  },

  template: `
  <div id="playlistitem" class="page">
    <header class="page-header">
      <button-tampan
        icon-text="arrow_back"
        @click="$router.go(-1)"
      ></button-tampan>
      <h2 class="page-title">Playlist: {{ playlist.name }}</h2>
      <div style="flex: 1;"></div>
      <button-tampan
        icon-text="edit"
        @click="openEditPlaylist = true"
      ></button-tampan>
    </header>
    <div class="page-content">
      <div class="player">
        <player-controls
          :md5="md5"
          :md5s="md5s"
          @play="isPlaying = true"
          @pause="isPlaying = false"
        ></player-controls>

        <div v-if="isAudioSelected" class="audio-controls">
          <button-tampan
            display="negative outline"
            icon-text="delete_forever"
            @click="deleteAudio"
          >Hapus</button-tampan>
          <div class="button-group align-right">
            <button-tampan
              icon-text="arrow_downward"
              @click="changeOrder(1)"
            >Turun</button-tampan>
            <button-tampan
              icon-text="arrow_upward"
              @click="changeOrder(-1)"
            >Naik</button-tampan>
          </div>
        </div>

        <list-block
          :src="playlist.audios"
          :query="'filename=%'+keyword+'%'"
          :number-of-column="numberOfColumn"
          :row-height="$tampan.client.isSmallScreen ? 72 : 80"
          :gap="4"
        >
          <div
            slot="content"
            slot-scope="{ data }"
            class="list-block-content"
            :class="
              (data.md5 === $tampan.md5 && ' active ')
              + (selectedAudioIndex === data.index && ' selected ')
            "
          >
            <div class="list-block-detail" @click="$tampan.playAudio(data.md5, md5s)">
              <div class="list-block-label">
                <span
                  v-show="data.md5 === $tampan.md5"
                  class="icon material-icons"
                >{{ isPlaying ? 'play_arrow' : 'pause' }}</span>
                {{ data.filename }}
              </div>
              <div class="list-block-description">{{ durationFormat(data.duration) }}</div>
            </div>
            <button-tampan
              :icon-text="selectedAudioIndex === data.index ? 'indeterminate_check_box' : 'info'"
              @click="selectedAudioIndex = selectedAudioIndex === data.index ? -1 : data.index"
            ></button-tampan>
          </div>
          <p
            slot="content-empty"
            style="
              font-style: italic;
              text-align: center;
              color: #616161;
              margin: 2rem 0;
            "
          >Materi tidak ditemukan.</p>
        </list-block>

        <button-action
          icon-text="library_add"
          @click="openAudioUpload = true"
        ></button-action>

      </div>
    </div>

    <audio-upload
      :show="openAudioUpload"
      @done="onAudioUploadDone"
      @close="openAudioUpload = false"
    ></audio-upload>

    <edit-playlist
      :id="numericId"
      :show="openEditPlaylist"
      @save="onPlaylistSaved"
      @delete="onPlaylistDeleted"
      @close="openEditPlaylist = false"
    ></edit-playlist>
  </div>
  `,
});
