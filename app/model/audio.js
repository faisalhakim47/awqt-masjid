/**
 * @param {number} input 
 */
export function durationFormat(input) {
  const milliseconds = input % 1000
  input = (input - milliseconds) / 1000
  let seconds = (input % 60)
  let minutes = ((input - seconds) / 60)
  seconds = seconds === 0 ? '' : seconds + 'd'
  minutes = minutes === 0 ? '' : minutes + 'm'
  return minutes + (seconds ? seconds : '')
}
