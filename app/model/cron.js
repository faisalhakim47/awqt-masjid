/**
 * @param {string} months 
 * @param {string} dates 
 * @param {string} days 
 * @param {string} minutes 
 * @param {string} seconds 
 */
export function cron(months, dates, days, minutes, seconds) {
  return {
    months,
    dates,
    days,
    minutes,
    seconds
  }
}
