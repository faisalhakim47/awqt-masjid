const Vue = require('vue').default
import { router } from './router.js'

export default () => new Vue({
  router,

  el: '#app',

  data() {
    return {
      menuGroups: [
        {
          name: 'Menu',
          menus: [
            {
              name: 'Beranda',
              route: { name: 'Dashboard' },
              iconText: 'dashboard',
              exact: true,
            },
            {
              name: 'Tutorial',
              route: { name: 'Tutorial' },
              iconText: 'book',
            },
            {
              name: 'Paket Jadwal',
              route: { name: 'SchedulePackList' },
              iconText: 'event',
            },
            {
              name: 'Materi Player',
              route: { name: 'MediaPlayer' },
              iconText: 'play_circle_filled',
            },
            {
              name: 'Speaker',
              route: { name: 'Speaker' },
              iconText: 'speaker',
            },
            {
              name: 'Wifi Password',
              route: { name: 'AccessPoint' },
              iconText: 'wifi_lock',
            },
            {
              name: 'Hari Jum\'at',
              route: { name: 'Jumat' },
              iconText: 'star',
            },
            {
              name: 'Wilayah Jadwal',
              route: { name: 'TimeZone' },
              iconText: 'map',
            },
            {
              name: 'Software',
              route: { name: 'Upgrade' },
              iconText: 'system_update',
            },
          ]
        },
      ],
    }
  },

  template: `
  <admin-panel :menu-groups="menuGroups">
    <header slot="header">
      <div class="brand">
        <img class="brand-logo" src="/static/images/mosque.svg">
        <h1 class="brand-name">Awqot</h1>
      </div>
    </header>
    <router-view slot="content"></router-view>
  </admin-panel>
  `
})
