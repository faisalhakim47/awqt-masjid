export function reportError(error) {
  console.log(error)
}

const errorDictionary = [
  {
    match: error =>
      error.errorInfo && error.errorInfo[2] === "Column 'nik' cannot be null",
    translation: {
      id: 'Anda belum mengisi salah satu kolom NIK.',
    },
  },
  {
    match: error =>
      error.errorInfo &&
      error.errorInfo[2] &&
      error.errorInfo[2].indexOf("Data too long for column 'nik'") === 0,
    translation: {
      id:
        'Salah satu kolom data NIK terlalu panjang. Jumlah karakter NIK harus pas 16 karakter.',
    },
  },
]

export function translateError(error, { lang = 'id' } = {}) {
  const errorMsg = errorDictionary.find(dict => dict.match(error))
  if (!errorMsg) return error
  return errorMsg.translation[lang]
}
