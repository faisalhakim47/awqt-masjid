import { request } from '../vue-tampan/index.js'
import { objectToQueryString } from './url.js'

const AWQT_SERVER = location.origin

/**
 * @template Val
 * @param {string} path
 * @param {object} options
 * @returns {Promise<>}
 */
export function api(path, options = {}) {
  const query = options.query ? '?' + objectToQueryString(options.query) : ''
  return request('POST', AWQT_SERVER + path + query, {
    ...options,
    headers: {
      ...(options.headers || {}),
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'App-Name': 'awqt',
      'App-Token': localStorage.getItem('awqt_token'),
    }
  }).then(({ status, data }) => {
    if (status === 200) {
      return data
    } else {
      throw data
    }
  })
}

export function setAPIToken(token) {
  localStorage.setItem('awqt_token', token)
}
