import Dashboard from './pages/Dashboard.js'
import SchedulePackList from './pages/SchedulePackList.js'
import MediaPlayer from './pages/MediaPlayer.js'
import PlaylistItem from './pages/PlaylistItem.js'
import Speaker from './pages/Speaker.js'
import AccessPoint from './pages/AccessPoint.js'
import Jumat from './pages/Jumat.js'
import TimeZone from './pages/TimeZone.js'
import Tutorial from './pages/Tutorial.js'
import Upgrade from './pages/Upgrade.js'

export const router = new VueRouter({
  mode: 'history',
  routes: [
    { name: 'Dashboard', path: '/', component: Dashboard },
    { name: 'SchedulePackList', component: SchedulePackList, path: '/schedule_packs' },
    { name: 'MediaPlayer', component: MediaPlayer, path: '/media_player' },
    { name: 'PlaylistItem', component: PlaylistItem, path: '/playlists/:id', props: true },
    { name: 'Speaker', component: Speaker, path: '/speaker' },
    { name: 'AccessPoint', component: AccessPoint, path: '/access_point' },
    { name: 'Jumat', component: Jumat, path: '/jumat' },
    { name: 'TimeZone', component: TimeZone, path: '/time_zone' },
    { name: 'Upgrade', component: Upgrade, path: '/upgrade' },
    { name: 'Tutorial', component: Tutorial, path: '/tutorial' },
    { path: '*', redirect: { name: 'Dashboard' } },
  ]
})
