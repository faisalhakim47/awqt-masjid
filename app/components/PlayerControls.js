const Vue = require('vue').default;
import { api } from '../tools/api.js';

export default Vue.extend({
  props: {
    md5: { type: String, required: true },
    md5s: { type: Array, required: true },
  },

  data() {
    return {
      audio: {},
    };
  },

  methods: {
    fetchAudio() {
      const fetching = api(
        '/awqt/api/get.audio-item.php?md5=' + this.$tampan.md5
      ).then((audio) => {
        this.$emit('audio', JSON.parse(JSON.stringify(audio)));
        this.audio = audio;
      });
      this.$tampan.useLoadingState(fetching);
      return;
    },
  },

  watch: {
    '$tampan.md5'() {
      this.fetchAudio();
    },
    '$tampan.isPlaying'(isPlaying) {
      if (isPlaying) this.$emit('play');
      else this.$emit('pause');
    },
  },

  mounted() {
    this.fetchAudio();
  },

  beforeDestroy() {
    this.$tampan.pauseAudio();
    if (this.$tampan.speakerMode === 'speaker') {
      this.$tampan.speakerOff();
    }
  },

  template: `
  <div class="player-controls">
    <div
      style="
        display: flex;
        justify-content: space-between;
      "
    >
      <div
        class="controls"
        style="
          display: inline-flex;
          align-items: center;
          justify-content: center;
        "
      >
        <button-tampan
          :icon-text="$tampan.speakerMode"
          @click="$tampan.changeSpeakerMode()"
        ></button-tampan>
        <button-tampan
          icon-text="skip_previous"
          @click="$tampan.backAudio()"
          ></button-tampan>
        <button-tampan
          v-if="$tampan.isPlaying"
          icon-text="pause_circle_filled"
          display="large"
          @click="$tampan.pauseAudio()"
        ></button-tampan>
        <button-tampan
          v-else-if="$tampan.isPaused"
          icon-text="play_circle_filled"
          display="large"
          @click="$tampan.resumeAudio()"
        ></button-tampan>
        <button-tampan
          v-else
          icon-text="play_circle_filled"
          display="large"
          @click="$tampan.playAudio(md5, md5s)"
        ></button-tampan>
        <button-tampan
          icon-text="skip_next"
          @click="$tampan.nextAudio()"
        ></button-tampan>
        <button-tampan
          :icon-text="$tampan.playMode"
          @click="$tampan.changePlayMode()"
        ></button-tampan>
      </div>
      <input-range
        prefix-icon-text="volume_up"
        style="max-width: 150px;"
        v-model="$tampan.volume"
        :min="0"
        :max="100"
        @input="(value) => $tampan.setVolume(value)"
      ></input-range>
    </div>
    <input-range
      style="width: 100%;"
      :min="0"
      :max="audio.duration || 0"
      :value="$tampan.currentTime"
      @input="(seek) => $tampan.seekAudio(seek)"
    ></input-range>
  </div>
  `,
});
