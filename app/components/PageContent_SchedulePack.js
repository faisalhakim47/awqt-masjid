const Vue = require('vue').default;
import { api } from '../tools/api.js';

import SchedulePackAudioTable from './Box_SchedulePackAudio.js';

export default Vue.extend({
  components: {
    SchedulePackAudioTable,
  },

  props: {
    name: { type: String, required: true },
  },

  data() {
    return {
      schedules: [],
      day: 1,
      dayOptions: [
        { value: 1, label: 'Ahad' },
        { value: 2, label: 'Senin' },
        { value: 3, label: 'Selasa' },
        { value: 4, label: 'Rabu' },
        { value: 5, label: 'Kamis' },
        { value: 6, label: "Jum'at" },
        { value: 7, label: 'Sabtu' },
      ],
      time: 1,
      timeOptions: [
        { value: 1, label: 'Subuh' },
        { value: 2, label: 'Dzuhur' },
        { value: 3, label: 'Ashar' },
        { value: 4, label: 'Maghrib' },
        { value: 5, label: 'Isya' },
      ],
    };
  },

  computed: {
    activeSchedule() {
      const name = `::${this.name}::${this.day}:${this.time}:`;
      return this.schedules.find((schedule) => {
        return schedule.name === name;
      });
    },
  },

  methods: {
    loadSchedules() {
      const loading = api('/awqt/api/get.schedule-list.php', {
        query: { keyword: `::${this.name}::%` },
      })
        .then((schedules) => {
          this.schedules = schedules;
        })
        .catch((error) => {
          this.$tampan.alert({
            text: 'Gagal Memuat Jadwal-jadwal',
            text: error,
          });
          console.warn(error);
        });
    },
  },

  watch: {
    name() {
      this.loadSchedules();
    },
  },

  mounted() {
    this.loadSchedules();
  },

  template: `
  <div class="schedule-pack page-content padding" style="padding-bottom: 5rem;">
    <tabs
      theme="primary"
      :options="dayOptions"
      :value="day"
      @change="id => day = id"
    ></tabs>
    <tabs
      theme="secondary"
      :options="timeOptions"
      :value="time"
      @change="id => time = id"
    ></tabs>

    <row>
      <column :width="{ sm: 1, md: 3/5, lg: 1/2 }">
        <schedule-pack-audio-table
          v-if="activeSchedule"
          :schedule_id="activeSchedule.id"
          :schedule_name="activeSchedule.name"
        ></schedule-pack-audio-table>
      </column>
    </row>

  </div>
  `,
});
