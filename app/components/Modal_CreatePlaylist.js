const Vue = require('vue').default
import { api } from '../tools/api.js'

export default Vue.extend({
  props: {
    show: { type: Boolean, default: false },
    showCancelButton: { type: Boolean, default: true },
  },

  data() {
    return {
      playlist: {
        name: '',
      },
    }
  },

  methods: {
    savePlaylist() {
      const saving = api('/awqt/api/post.playlist-item.php', { data: { name: this.playlist.name } })
        .then(() => {
          this.$emit('created')
          this.$emit('close')
          this.playlist.name = ''
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Menyimpan',
            text: JSON.stringify(error),
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(saving)
    },
  },

  template: `
  <modal :show="show" @close="$emit('close')">
    <header slot="header" class="modal-header">
      <h3 class="modal-title">Buat Playlist</h3>
    </header>
    <field label="Nama Playlist">
      <input type="text" v-model="playlist.name">
    </field>
    <footer slot="footer" class="modal-footer">
      <div class="button-group" :class="showCancelButton ? 'justify-between' : 'align-right'">
        <button-tampan v-if="showCancelButton" icon-text="close" @click="$emit('close')">Batal</button-tampan>
        <button-tampan color="positive" icon-text="save" @click="savePlaylist">Simpan</button-tampan>
      </div>
    </footer>
  </modal>
  `
})
