const Vue = require('vue').default;
import { api } from '../tools/api.js';
import { durationFormat } from '../model/audio.js';

import AudioUpload from './Modal_AudioUpload.js';
import PlayerControls from './PlayerControls.js';

export default Vue.extend({
  props: {
    playlistId: { type: [Number, String] },
  },

  components: {
    AudioUpload,
    PlayerControls,
  },

  data() {
    return {
      openAudioUpload: false,
      isPlaying: false,
      md5: '',
      md5s: [],
      playlist: {
        name: '-',
        audios: [],
      },
      keyword: '',
    };
  },

  computed: {
    isPlaylist() {
      return !!this.playlistId;
    },

    filteredPlaylistAudios() {
      const rx = new RegExp(this.keyword, 'i');
      return this.playlist.audios.filter((audio) => {
        return rx.test(audio.filename);
      });
    },

    numberOfColumn() {
      if (this.$tampan.client.isSmallScreen) return 1;
      else if (this.$tampan.client.isMediumScreen) return 2;
      else return 3;
    },
  },

  methods: {
    durationFormat,

    fetchMd5s() {
      const request = this.isPlaylist
        ? api('/api/get.playlist_audio-list.php?id=' + this.playlistId).then(
            (playlist) => {
              this.playlist = playlist;
              return playlist.audios.map((audio) => {
                return audio.md5;
              });
            }
          )
        : api('/api/get.audio_md5-list.php');
      request
        .then((md5s) => {
          this.md5 = md5s[0] || '';
          this.md5s = md5s;
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memuat Daftar Audio',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(request);
    },

    onAudioUploaded(audios) {
      const posting = api('/awqt/api/patch.playlist-item.php', {
        data: {
          id: this.playlistId,
          name: this.playlist.name,
          audio_md5s: JSON.stringify([
            ...this.playlist.audios.map((audio) => audio.md5),
            ...audios.map((audio) => audio.md5),
          ]),
        },
      }).then(this.fetchMd5s);
      this.$tampan.useLoadingState(posting);
    },
  },

  watch: {
    playlistId() {
      this.fetchMd5s();
    },
  },

  mounted() {
    this.fetchMd5s();
  },

  template: `
  <div class="player">
    <player-controls
      :md5="md5"
      :md5s="md5s"
      @play="isPlaying = true"
      @pause="isPlaying = false"
    ></player-controls>
    <list-block
      :src="filteredPlaylistAudios"
      :query="'filename=%'+keyword+'%'"
      :number-of-column="numberOfColumn"
      :row-height="$tampan.client.isSmallScreen ? 72 : 80"
    >
      <div
        slot="content"
        slot-scope="{ data }"
        class="list-block-content"
        :class="data.md5 === $tampan.md5 && 'active'"
      >
        <div class="list-block-detail" @click="$tampan.playAudio(data.md5, md5s)">
          <div class="list-block-label">
            <span
              v-show="data.md5 === $tampan.md5"
              class="icon material-icons"
            >{{ isPlaying ? 'play_arrow' : 'pause' }}</span>
            {{ data.filename }}
          </div>
          <div class="list-block-description">{{ durationFormat(data.duration) }}</div>
        </div>
        <button-tampan
          icon-text="info"
          @click="$tampan.playAudio(data.md5, md5s)"
        ></button-tampan>
      </div>
      <p
        slot="content-empty"
        style="
          font-style: italic;
          text-align: center;
          color: #616161;
          margin: 2rem 0;
        "
      >Materi tidak ditemukan.</p>
    </list-block>

    <button-action
      icon-text="library_add"
      @click="openAudioUpload = true"
    ></button-action>

    <audio-upload
      :show="openAudioUpload"
      :upload-only="!isPlaylist"
      @done="onAudioUploaded"
      @close="openAudioUpload = false"
    ></audio-upload>
  </div>
  `,
});
