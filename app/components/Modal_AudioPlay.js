const Vue = require('vue').default;
import { api } from '../tools/api.js';
import PlayerControls from '../components/PlayerControls.js';

export default Vue.extend({
  props: {
    show: { type: [Boolean] },
    md5s: { type: [Array], default: () => [] },
  },

  components: {
    PlayerControls,
  },

  data() {
    return {
      md5: '',
      isPlaying: false,
      openPlayAwqt: false,
      openPlayDevice: false,
      audio: {},
    };
  },

  methods: {
    fetchAudios() {
      const fetching = api('/awqt/api/get.audio-list.php', {
        query: { md5s: this.md5s.join(',') },
      })
        .then((audios) => {
          this.audios = audios;
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memuat Data Audio',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(fetching);
      return fetching;
    },
  },

  watch: {
    md5s() {
      this.md5 = this.md5s[0];
      if (this.show) this.fetchAudios();
    },
    show() {
      this.fetchAudios();
    },
  },

  template: `
  <modal :show="show" @close="$emit('close')">
    <player-controls
      :md5="md5"
      :md5s="md5s"
      @play="isPlaying = true"
      @pause="isPlaying = false"
      @audio="_ => audio = _"
    ></player-controls>

    <p style="text-align: center; font-style: italic;">{{ audio ? audio.filename : '' }}</p>

    <footer slot="footer" class="modal-footer">
      <div class="button-group">
        <div style="flex: 1;"></div>
        <button-tampan
          icon-text="close"
          @click="$emit('close')"
        >Tutup</button-tampan>
      </div>
    </footer>
  </modal>
  `,
});
