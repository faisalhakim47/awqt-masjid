const Vue = require('vue').default;
import { api } from '../tools/api.js';
import { durationFormat } from '../model/audio.js';

import AudioUpload from './Modal_AudioUpload.js';
import AudioPlay from './Modal_AudioPlay.js';

export default Vue.extend({
  components: {
    AudioUpload,
    AudioPlay,
  },

  props: {
    schedule_id: { type: Number, required: true },
    schedule_name: { type: String, required: true },
  },

  data() {
    return {
      isOrdering: false,
      isDeleting: false,
      openAudioUpload: false,
      openModalAudioPlay: false,
      box_content: null,
      audioTask: {},
      audios: [],
      selectedAudios: [],
    };
  },

  computed: {
    audioControlsStyle() {
      if (this.$tampan.client.isLargeScreen && this.box_content) {
        return {
          width: this.box_content.offsetWidth - 6 + 'px',
          left: this.box_content.offsetLeft - 1 + 'px',
          bottom: '1rem',
          border: '1px solid #E0E0E0',
          boxShadow:
            '0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3)',
        };
      }
      return {};
    },

    totalDuration() {
      return this.audios
        .map((audio) => audio.duration)
        .reduce((sum, duration) => sum + duration, 0);
    },

    audioMd5s() {
      if (this.selectedAudios.length === 0)
        return this.audios.map((audio) => audio.md5);
      return this.selectedAudios.map((audio) => audio.md5);
    },
  },

  methods: {
    durationFormat,

    loadAudios() {
      const { schedule_id } = this;
      if (!schedule_id) return;
      if (typeof schedule_id !== 'number') return;
      const loading = api('/awqt/api/get.schedule_task-item-by-schedule.php', {
        query: { schedule_id },
      })
        .then((tasks) => {
          this.audioTask = tasks.find((task) => {
            return task.name === 'AudioPlayUsingSpeaker';
          });
          const { md5s } = JSON.parse(this.audioTask.payload);
          Promise.all(
            md5s.map((md5) => {
              return api(`/awqt/api/get.audio-item.php`, { query: { md5 } });
            })
          ).then((audios) => {
            this.audios = audios;
          });
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memuat Audio-audio',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(loading);
    },

    doneAudioUpload(audios) {
      const data = {
        ...this.audioTask,
        payload: JSON.stringify({
          md5s: [
            ...this.audios.map((audio) => audio.md5),
            ...audios.map((audio) => audio.md5),
          ],
        }),
      };
      const saving = api('/awqt/api/patch.schedule_task-item.php', { data })
        .then(this.loadAudios)
        .then(() => api('/awqt/api/post.compute_next.php'))
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Mentimpan Audio',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(saving);
    },

    toggleSelect(audio) {
      // const index = this.selectedAudios.indexOf(audio)
      // if (index === -1) this.selectedAudios = [audio]
      // else this.selectedAudios = []
      const index = this.selectedAudios.indexOf(audio);
      if (index === -1) {
        this.selectedAudios.push(audio);
      } else {
        this.selectedAudios.splice(index, 1);
      }
    },

    changeOrder(offset) {
      if (this.isOrdering) return;
      this.isOrdering = true;
      const audios = [...this.audios];
      const selectedAudios =
        offset > 0 ? [...this.selectedAudios].reverse() : this.selectedAudios;
      selectedAudios.forEach((audio) => {
        const audioIndex = audios.indexOf(audio);
        const targetIndex = audioIndex + offset;
        if (targetIndex < 0) return;
        const movedAudio = audios.splice(audioIndex, 1);
        audios.splice(audioIndex + offset, 0, ...movedAudio);
      });
      this.audios = audios;
      const data = {
        ...this.audioTask,
        payload: JSON.stringify({
          md5s: this.audios.map(({ md5 }) => md5),
        }),
      };
      const ordering = api('/awqt/api/patch.schedule_task-item.php', { data })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Mengubah Urutan Audio',
            text: error,
          });
          console.warn(error);
        })
        .then(() => (this.isOrdering = false))
        .then(() => api('/awqt/api/post.compute_next.php'));
      this.$tampan.useLoadingState(ordering);
    },

    deleteAudio() {
      if (this.isDeleting) return;
      this.$tampan
        .confirm({
          title: 'Anda yakin menghapus audio ini?',
          confirmText: 'Hapus',
          confirmIconText: 'delete_forever',
        })
        .then(() => {
          this.isDeleting = true;
          this.selectedAudios.forEach((audio) => {
            this.audios.splice(this.audios.indexOf(audio), 1);
          });
          this.selectedAudios = [];
          const data = {
            ...this.audioTask,
            payload: JSON.stringify({
              md5s: this.audios.map(({ md5 }) => md5),
            }),
          };
          const deleting = api('/awqt/api/patch.schedule_task-item.php', {
            data,
          })
            .then(this.loadAudios)
            .catch((error) => {
              this.$tampan.alert({
                title: 'Gagal Menghapus Audio',
                text: error,
              });
              console.warn(error);
            })
            .then(() => (this.isDeleting = false))
            .then(() => api('/awqt/api/post.compute_next.php'));
          this.$tampan.useLoadingState(deleting);
        });
    },
  },

  watch: {
    schedule_id() {
      this.loadAudios();
    },
  },

  mounted() {
    this.loadAudios();
    this.box_content = this.$refs.box_content;
  },

  template: `
  <div class="box schedulepackaudio">
    <div class="box-header" style="display: flex; justify-content: space-between;">
      <h3 class="box-title">Audio-audio</h3>
      <div>
        <button-tampan
          icon-text="play_arrow"
          @click="openModalAudioPlay = true"
        >Mainkan</button-tampan>
        <button-tampan
          display="positive"
          icon-text="add"
          @click="openAudioUpload = true"
        >Tambah</button-tampan>
      </div>
    </div>
    <div ref="box_content" class="box-content">
      <table class="table audios-list borderless ellipsis">
        <tbody>
          <tr
            v-for="audio in audios"
            :class="selectedAudios.indexOf(audio) !== -1 && 'active'"
            @click="toggleSelect(audio)"
          >
            <td
              style="
                text-align: center;
                width: 4rem;
                font-variant-numeric: tabular-nums;
              "
            >{{ durationFormat(audio.duration) }}</td>
            <td
              :style="{
                'max-width': $tampan.client.isSmallScreen ? '10rem' : '15rem'
              }"
            >{{ audio.filename }}</td>
            <td style="text-align: right; width: 1px;">
              <button-tampan v-if="selectedAudios.indexOf(audio) === -1" icon-text="edit"></button-tampan>
              <button-tampan v-else icon-text="indeterminate_check_box"></button-tampan>
            </td>
          </tr>
          <tr v-if="!audios.length">
            <td class="emptyinfo" colspan="3">
              Belum ada audio yang ditambahkan. Silahkan upload audio ke jadwal ini.
            </td>
          </tr>
        </tbody>
      </table>

      <p v-if="totalDuration !== 0">total durasi {{ durationFormat(totalDuration) }}</p>

      <div  v-if="selectedAudios.length" class="audio-controls" :style="audioControlsStyle">
        <div class="button-group justify-between">
          <button-tampan
            color="negative outline"
            icon-text="delete_forever"
            :disabled="isDeleting" @click="deleteAudio"
          >Hapus</button-tampan>
          <div class="button-group align-right">
            <button-tampan
              v-if="selectedAudios.length === 1"
              icon-text="arrow_downward"
              :disabled="isOrdering"
              @click="changeOrder(1)"
            >Turun</button-tampan>
            <button-tampan
              v-if="selectedAudios.length === 1"
              icon-text="arrow_upward"
              :disabled="isOrdering"
              @click="changeOrder(-1)"
            >Naik</button-tampan>
          </div>
        </div>
      </div>

    </div>

    <audio-upload
      :show="openAudioUpload"
      @done="doneAudioUpload"
      @close="openAudioUpload = false"
    ></audio-upload>

    <audio-play
      :show="openModalAudioPlay"
      :md5s="audioMd5s"
      @close="openModalAudioPlay = false"
    ></audio-play>
  </div>
  `,
});
