const Vue = require('vue').default;
import { api } from '../tools/api.js';
import { durationFormat } from '../model/audio.js';

export default Vue.extend({
  props: {
    show: { type: Boolean },
  },

  data() {
    return {
      audios: [],
      selectedAudios: [],
      keyword: '',
    };
  },

  computed: {
    filteredAudios() {
      const rx = new RegExp(this.keyword, 'i');
      return this.audios.filter((audio) => {
        return rx.test(audio.filename);
      });
    },
  },

  methods: {
    durationFormat,

    loadAudios() {
      const loading = api('/awqt/api/get.audio-list.php?get=data')
        .then((audios) => {
          this.audios = audios.map((audio) => {
            return {
              md5: audio.md5,
              filename: audio.filename,
              duration: durationFormat(audio.duration),
            };
          });
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal memuat data audio.',
            text: error,
          });
          console.warn(error);
        });
      this.$tampan.useLoadingState(loading);
    },

    toggleSelect(audio) {
      const index = this.selectedAudios.indexOf(audio);
      if (index === -1) {
        this.selectedAudios.push(audio);
      } else {
        this.selectedAudios.splice(index, 1);
      }
    },

    done() {
      this.$emit('done', this.selectedAudios);
      this.$emit('close');
    },
  },

  mounted() {
    this.loadAudios();
  },

  template: `
  <modal :show="show" @close="$emit('close')">
    <header slot="header" class="modal-header">
      <field style="width: 100%;">
        <input type="text" v-focus v-model="keyword" placeholder="Pilih Audio, cari disini...">
      </field>
    </header>

    <table class="table borderless">
      <tbody>
        <tr
          v-for="audio in filteredAudios"
          :key="audio.md5"
          :class="'audio-item' + (selectedAudios.indexOf(audio) === -1 ? '' : ' selected')"
          @click="toggleSelect(audio)"
        >
          <td style="font-variant-numeric: tabular-nums;">{{ audio.duration }}</td>
          <td>{{ audio.filename }}</td>
        </tr>
      </tbody>
    </table>

    <footer slot="footer" class="modal-footer">
      <div class="button-group align-right">
        <button-tampan icon-text="close" @click="$emit('close')">Batal</button-tampan>
        <button-tampan icon-text="check" @click="done">Selesai</button-tampan>
      </div>
    </footer>
  </modal>
  `,
});
