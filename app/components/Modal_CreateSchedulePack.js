const Vue = require('vue').default
import { days } from '../vue-tampan/index.js'
import { api } from '../tools/api.js'
import { cron } from '../model/cron.js'

/**
 * @param {string} tagName 
 */
function createSchedulePack(name) {
  const tagName = `:${name}:`
  const sholatTimes = [
    'Subuh',
    'Dzuhur',
    'Ashar',
    'Maghrib',
    'Isya',
  ]
  return sholatTimes
    .reduce((names, sholat, timeIndex) => {
      return names.concat(days.map((day, dayIndex) => ({
        scheduleName: `:${tagName}:${dayIndex + 1}:${timeIndex + 1}:`,
        day: dayIndex + 1,
        time_id: timeIndex + 1,
      })))
    }, [])
    .map(({ scheduleName, day, time_id }) => {
      return {
        name: scheduleName,
        tags: [tagName],
        audios: [],
        include_times: [{ time_id }],
        intersect_crons: [cron('*', '*', day.toString(), '*', '*')],
        exclude_crons: [],
      }
    })
}

export default Vue.extend({
  props: {
    show: { type: Boolean, default: false },
    cancelable: { type: Boolean, default: true },
  },

  data() {
    return {
      pack: {
        name: '',
      },
    }
  },

  methods: {
    saveSchedulePack() {
      const packName = this.pack.name

      if (!packName) return this.$tampan.alert({
        title: 'Kesalahan',
        text: 'Anda belum mengisi nama paket.',
      })

      const tagName = `:${packName}:`
      const saving = api('/awqt/api/get.tag-item.php', { query: { name: tagName } })
        .then((tag) => {
          if (tag) return this.$tampan.alert({
            titme: 'Terdapat Kesalahan',
            text: 'Nama paket harus unik. Terdapat paket jadwal lain yang sudah memiliki nama paket tersebut.'
          })

          api('/awqt/api/get.tag-list.php', { query: { name: ':%:', is_active: 1 } })
            .then((tags) => !tags.length ? 1 : 0)
            .then((isActive) => {
              return Promise.all(createSchedulePack(packName)
                .map((schedule) => {
                  return api('/awqt/api/post.schedule-item.php', { data: schedule })
                }))
                .then(() => isActive)
            })
            .then((is_active) => {
              return api('/awqt/api/patch.tag-item.php', { data: { name: tagName, is_active } })
            })
            .then(() => {
              this.$emit('created', { name: tagName })
            })
            .catch((error) => {
              this.$tampan.alert({
                title: 'Gagal Menyimpan',
                text: JSON.stringify(error)
              })
              console.warn(error)
            })
            .then(() => this.pack.name = '')
        })

      this.$tampan.useLoadingState(saving)
    }
  },

  template: `
  <modal :show="show" @close="$emit('close')">
    <header slot="header" class="modal-header">
      <h3 class="modal-title">Buat Paket Jadwal</h3>
    </header>
    <field label="Nama Paket">
      <input type="text" v-model="pack.name">
    </field>
    <footer slot="footer" class="modal-footer">
      <div class="button-group" :class="cancelable ? 'justify-between' : 'align-right'">
        <button-tampan v-if="cancelable" icon-text="close" @click="$emit('close')">Batal</button-tampan>
        <button-tampan color="positive" icon-text="save" @click="saveSchedulePack">Simpan</button-tampan>
      </div>
    </footer>
  </modal>
  `
})
