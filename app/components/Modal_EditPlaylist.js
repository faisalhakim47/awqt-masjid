const Vue = require('vue').default;
import { api } from '../tools/api.js';

export default Vue.extend({
  props: {
    id: { type: [Number], required: true },
    show: { type: Boolean, default: false },
  },

  data() {
    return {
      isLoading: false,
      playlist: {
        name: '',
      },
    };
  },

  methods: {
    fetchPlaylist() {
      this.isLoading = true;
      const query = { id: this.id };
      const fetching = api('/awqt/api/get.playlist-item.php', { query })
        .then((playlist) => {
          this.playlist = playlist;
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memuat Data Playlist',
            text: JSON.stringify(error),
          });
          console.warn(error);
        })
        .then(() => (this.isLoading = false));
      this.$tampan.useLoadingState(fetching);
      return fetching;
    },

    savePlaylist() {
      this.isLoading = true;
      const data = JSON.parse(JSON.stringify(this.playlist));
      const saving = api('/awqt/api/post.playlist-item.php', { data })
        .then(() => {
          this.$emit('save');
          this.$emit('close');
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Menyimpan Data Playlist',
            text: JSON.stringify(error),
          });
          console.warn(error);
        })
        .then(() => (this.isLoading = false));
      this.$tampan.useLoadingState(saving);
      return saving;
    },

    deletePlaylist() {
      return this.$tampan
        .confirm({
          title: 'Anda yakin ingin menghapus playlist ini?',
        })
        .then(() => {
          this.isLoading = true;
          const query = { id: this.id };
          const deleting = api('/awqt/api/delete.playlist-item.php', { query })
            .then(() => {
              this.$emit('close');
              return this.$nextTick();
            })
            .then(() => {
              this.$emit('delete');
            })
            .catch((error) => {
              this.$tampan.alert({
                title: 'Gagal Menghapus Playlist',
                text: JSON.stringify(error),
              });
              console.warn(error);
            })
            .then(() => (this.isLoading = false));
          this.$tampan.useLoadingState(deleting);
          return deleting;
        });
    },
  },

  mounted() {
    this.fetchPlaylist();
  },

  template: `
  <modal :show="show" @close="$emit('close')">
    <header slot="header" class="modal-header">
      <h3 class="modal-title">Buat Playlist</h3>
    </header>
    <field label="Nama Playlist">
      <input type="text" v-model="playlist.name" :disabled="isLoading">
    </field>
    <footer slot="footer" class="modal-footer">
      <div class="button-group justify-between">
        <button-tampan
          icon-text="close"
          @click="$emit('close')"
        >Batal</button-tampan>
        <div>
          <button-tampan
            display="negative outline"
            icon-text="delete_forever"
            @click="deletePlaylist"
          >Hapus</button-tampan>
          <button-tampan
            display="positive"
            icon-text="save"
            @click="savePlaylist"
          >Simpan</button-tampan>
        </div>
      </div>
    </footer>
  </modal>
  `,
});
