const Vue = require('vue').default

import AudioSelector from './Modal_AudioSelector.js'

export default Vue.extend({
  props: {
    show: { type: Boolean, default: false },
    uploadOnly: { type: Boolean, default: false },
  },

  components: {
    AudioSelector,
  },

  data() {
    return {
      openAudioSelector: false,
      audios: [],
    }
  },

  methods: {
    uploadAudios(event) {
      /** @type {File[]} */
      const audios = Array.from(event.target.files)

      this.audios = audios.map((audio) => {
        return {
          filename: audio.name,
          progress: 0,
        }
      })

      Promise
        .all(audios.map((audio, index) => new Promise((resolve, reject) => {
          const req = new XMLHttpRequest()
          req.open('POST', '/awqt/api/post.audio-item.php?filename=' + audio.name, true)
          const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'App-Name': 'awqt',
            'App-Token': localStorage.getItem('awqt_token'),
          }
          Object.keys(headers).forEach((header) => {
            req.setRequestHeader(header, headers[header])
          })
          req.upload.addEventListener('progress', (progress) => {
            this.audios[index].progress = (progress.loaded / progress.total * 100)
              .toString()
              .slice(0, 5)
          })
          req.addEventListener('error', (error) => {
            reject(error)
          })
          req.addEventListener('loadend', () => {
            this.audios[index].progress = 100
            resolve(JSON.parse(req.responseText))
          })
          req.send(audio)
        })))
        .then((audios) => {
          this.$emit('done', audios)
          this.$emit('close')
          this.audios = []
        })
        .catch((error) => {
          this.$tampan.alert({
            text: 'Gagal Menunggah Audio',
            text: error,
          })
          console.warn(error)
        })
    },

    audioSelectorDone(audios) {
      this.$emit('done', audios)
      this.$emit('close')
    },
  },

  watch: {
    show(show) {
      if (show && this.uploadOnly) {
        this.$nextTick().then(() => {
          this.$refs.audio_upload.click()
        })
      }
    }
  },

  template: `
  <modal :show="show" class="audio-upload" @close="$emit('close')">
    <header slot="header" class="modal-header">
      <h3 class="modal-title">Tambah Materi Audio</h3>
      <button-tampan icon-text="close" @click="$emit('close')"></button-tampan>
    </header>

    <input
      ref="audio_upload"
      type="file"
      accept="audio/*"
      multiple
      @change="uploadAudios"
      style="display: none;">

    <div
      v-if="!audios.length"
      class="button-group justify-around"
      style="padding: 2rem 1rem;"
    >
      <button-tampan
        icon-text="smartphone"
        @click="$refs.audio_upload.click()"
      >Dari Smartphone</button-tampan>
      <button-tampan
        v-if="!uploadOnly"
        icon-text="sd_card"
        @click="openAudioSelector = true"
      >Dari Awqot</button-tampan>
    </div>

    <table v-if="audios.length" class="table">
      <tbody>
        <tr v-for="audio in audios">
          <td>{{ audio.filename }}</td>
          <td>
            <span v-if="audio.progress != 100">{{ audio.progress }}%</span>
            <span v-else class="icon material-icons">check</span>
          </td>
        </tr>
      </tbody>
    </table>

    <audio-selector
      :show="openAudioSelector"
      @done="audioSelectorDone"
      @close="openAudioSelector = false"
    ></audio-selector>
  </modal>
  `
})
