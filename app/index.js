const Vue = require('vue').default
import { VueTampan } from './vue-tampan/index.js'
import initApplication from './App.js'
import { api } from './tools/api.js'

const audioElement = new Audio()

Vue.use(VueTampan, {
  data() {
    return {
      isFetchAwqtPlayerState: false,
      isPlaying: false,
      isPaused: false,
      volume: 100,
      currentTime: 0,
      md5: null,
      md5s: [],
      speakerMode: 'smartphone',
      speakerModeList: ['smartphone', 'speaker'],
      playMode: 'repeat',
      playModeList: ['repeat', 'repeat_one', 'shuffle'],
    }
  },

  methods: {
    changeSpeakerMode() {
      this.speakerMode = findNext(
        this.speakerMode, this.speakerModeList, 1
      )
    },

    changePlayMode() {
      this.playMode = findNext(
        this.playMode, this.playModeList, 1
      )
    },

    fetchMediaPlayerState() {
      return api('/awqt/api/get.mediaplayer_state.php')
        .then((state = {}) => {
          if (this.speakerMode === 'speaker') {
            if (typeof state.filename === 'string') {
              const parts = state.filename.split('/')
              const md5 = parts[parts.length - 1]
              if (md5) this.md5 = md5
            }
            this.isPlaying = state.playing
            this.isPaused = state.filename && !state.playing
            this.currentTime = parseInt(state.position, 10) * 1000
          }
        })
    },

    recursiveFetchAwqtPlayerState() {
      if (this.isFetchAwqtPlayerState) return
      this.isFetchAwqtPlayerState = true
      const fetchAwqtPlayerState = () => this.fetchMediaPlayerState()
        .then(() => {
          if (this.speakerMode === 'speaker') {
            setTimeout(fetchAwqtPlayerState, 500)
          }
          else {
            this.isFetchAwqtPlayerState = false
          }
        })
      fetchAwqtPlayerState()
    },

    fetchAwqtPlayerState() {
      return this.useLoadingState(Promise.all([
        api('/awqt/api/get.volume.php')
          .then((result) => {
            this.volume = result.volume
          }),
        this.recursiveFetchAwqtPlayerState(),
      ]))
    },

    setVolumeOnAwqt(volume = 100) {
      return api('/awqt/api/post.volume.php?vol=' + volume)
    },

    playAudioOnAwqt(md5) {
      const payload = JSON.stringify({ md5s: [md5] })
      return api('/awqt/api/post.execute_task.php?name=MediaPlay&payload=' + payload)
        .then(this.recursiveFetchAwqtPlayerState)
    },

    pauseAudioOnAwqt() {
      return api('/awqt/api/post.execute_task.php?name=MediaPause')
    },

    stopAudioOnAwqt() {
      return api('/awqt/api/post.execute_task.php?name=MediaStop')
    },

    resumeAudioOnAwqt() {
      return api('/awqt/api/post.execute_task.php?name=MediaResume')
        .then(this.recursiveFetchAwqtPlayerState)
    },

    seekAudioOnAwqt(seek) {
      const payload = JSON.stringify({ seek })
      return api('/awqt/api/post.execute_task.php?name=MediaSeek&payload=' + payload)
        .then(this.recursiveFetchAwqtPlayerState)
    },

    setVolumeOnLocal(volume) {
      audioElement.volume = parseInt(volume, 10) / 100
    },

    playAudioOnLocal(md5) {
      audioElement.pause()
      audioElement.src = `/awqt/data/audios/${md5}`
      audioElement.currentTime = 0
      audioElement.play()
    },

    pauseAudioOnLocal() {
      audioElement.pause()
    },

    stopAudioOnLocal() {
      this.pauseAudioOnLocal()
      audioElement.currentTime = 0
    },

    resumeAudioOnLocal() {
      audioElement.play()
    },

    seekAudioOnLocal(seek) {
      audioElement.currentTime = seek / 1000
    },

    setVolume(volume) {
      if (this.speakerMode === 'smartphone') {
        this.setVolumeOnLocal(volume)
      }
      else if (this.speakerMode === 'speaker') {
        this.setVolumeOnAwqt(volume)
      }
    },

    playAudio(md5, md5s = [md5]) {
      this.md5 = md5
      this.md5s = md5s
      if (this.speakerMode === 'smartphone') {
        this.playAudioOnLocal(this.md5)
      }
      else if (this.speakerMode === 'speaker') {
        this.playAudioOnAwqt(this.md5)
      }
    },

    pauseAudio() {
      if (this.speakerMode === 'smartphone') {
        this.pauseAudioOnLocal()
      }
      else if (this.speakerMode === 'speaker') {
        this.pauseAudioOnAwqt()
      }
    },

    resumeAudio() {
      if (this.speakerMode === 'smartphone') {
        this.resumeAudioOnLocal()
      }
      else if (this.speakerMode === 'speaker') {
        this.resumeAudioOnAwqt()
      }
    },

    seekAudio(seek) {
      if (this.speakerMode === 'smartphone') {
        this.seekAudioOnLocal(seek)
      }
      else if (this.speakerMode === 'speaker') {
        this.seekAudioOnAwqt(seek)
      }
    },

    shuffleAudio() {
      const randomIndex = Math.floor(Math.random() * this.md5s.length)
      this.playAudio(this.md5s[randomIndex], this.md5s)
    },

    nextAudio() {
      switch (this.playMode) {
        case 'repeat':
          const nextAudio = findNext(this.md5, this.md5s, 1)
          this.playAudio(nextAudio, this.md5s)
          break
        case 'repeat_one':
          this.playAudio(this.md5, this.md5s)
          break
        case 'shuffle':
          this.shuffleAudio()
          break
      }
    },

    backAudio() {
      switch (this.playMode) {
        case 'repeat':
          const nextAudio = findNext(this.md5, this.md5s, -1)
          this.playAudio(nextAudio, this.md5s)
          break
        case 'repeat_one':
          this.playAudio(this.md5, this.md5s)
          break
        case 'shuffle':
          this.shuffleAudio()
          break
      }
    },

    speakerOn() {
      const updating = api('/awqt/api/post.execute_task.php?name=SpeakerOn')
      this.$tampan.useLoadingState(updating)
    },

    speakerOff() {
      const updating = api('/awqt/api/post.execute_task.php?name=SpeakerOff')
      this.$tampan.useLoadingState(updating)
    },
  },

  watch: {
    speakerMode(speakerMode) {
      if (speakerMode === 'smartphone') {
        this.speakerOff()
        this.stopAudioOnAwqt()
        if (audioElement.src) {
          const pathParts = audioElement.src.split('/')
          this.md5 = pathParts[pathParts.length - 1]
        }
        this.volume = audioElement.volume * 100
        this.currentTime = audioElement.currentTime * 1000
        this.isPlaying = audioElement.currentTime > 0
          && !audioElement.paused
          && !audioElement.ended
          && audioElement.readyState > 2
        this.isPaused = audioElement.paused
      }
      else if (speakerMode === 'speaker') {
        this.speakerOn()
        this.pauseAudioOnLocal()
        this.fetchAwqtPlayerState()
      }
    },
  },

  created() {
    audioElement.addEventListener('play', () => {
      if (this.speakerMode === 'smartphone') {
        this.isPlaying = true
        this.isPaused = false
      }
    })
    audioElement.addEventListener('pause', () => {
      if (this.speakerMode === 'smartphone') {
        this.isPlaying = false
        this.isPaused = true
      }
    })
    audioElement.addEventListener('timeupdate', () => {
      if (this.speakerMode === 'smartphone') {
        this.currentTime = audioElement.currentTime * 1000
      }
    })
  },
})

function findNext(item, items, move) {
  const nextIndex = items.indexOf(item) + move
  if (nextIndex >= items.length) {
    return items[0]
  }
  if (nextIndex < 0) {
    return items[items.length - 1]
  }
  return items[nextIndex]
}

initApplication()
