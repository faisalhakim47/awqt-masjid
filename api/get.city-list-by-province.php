<?php

require_once __DIR__ . "/app.php";

$province = require_querystring("province");

$dir = __DIR__ . "/import-time/{$province}";

if ($handle = opendir($dir)) {
  $entries = [];
  while (false !== ($entry = readdir($handle))) {
    if ($entry === ".") continue;
    if ($entry === "..") continue;
    array_push($entries, str_replace(".php", "", $entry) );
  }
  send_json(200, $entries);
} else {
  send_json(500, ["msg" => "province not found"]);
}
