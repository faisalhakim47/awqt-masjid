<?php

require_once __DIR__ . "/app.php";

$dir = __DIR__ . "/import-time";

if ($handle = opendir($dir)) {
  $entries = [];
  while (false !== ($entry = readdir($handle))) {
    if ($entry === ".") continue;
    if ($entry === "..") continue;
    array_push($entries, $entry);
  }
  send_json(200, $entries);
} else {
  send_json(500, ["msg" => "directory not found"]);
}
