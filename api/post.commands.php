<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/../awqt/api/model/awqt.php";

$command = require_querystring("name");

$result = null;

function accept_command() {
  pin_toggle(27);
  sleep(1);
  pin_toggle(27);
}

if ($command === "rmd") {
  $result = [];
  $result["deactivates"] = execute_sql("
    UPDATE tags
    SET is_active = 0
  ")->fetchAll();
  $result["activates"] = execute_sql("
    UPDATE tags
    SET is_active = 1
    WHERE name = ':Ramadan:'
  ")->fetchAll();
  accept_command();
}

else if ($command === "hba") {
  $result = [];
  $result["deactivates"] = execute_sql("
    UPDATE tags
    SET is_active = 0
  ")->fetchAll();
  $result["activates"] = execute_sql("
    UPDATE tags
    SET is_active = 1
    WHERE name = ':Hari Biasa:'
  ")->fetchAll();
  accept_command();
}

else if ($command === "tst") {
  schedule_test();
  accept_command();
}

else if ($command === "cmp") {
  compute_next();
  accept_command();
}

else if ($command === "stp") {
  audio_stop();
  accept_command();
}

else if ($command === "s") {
  speaker_toggle();
}

send_json(200, ["result" => $result]);
