<?php

function get_dir($dir)
{
  if ($handle = opendir($dir)) {
    $entries = [];
    while (false !== ($entry = readdir($handle))) {
      if ($entry === ".") {
        continue;
      }

      if ($entry === "..") {
        continue;
      }

      $fullname = $dir . "/" . $entry;
      array_push($entries, [
        "name" => $entry,
        "fullname" => $fullname,
        "is_dir" => is_dir($fullname),
      ]);
    }
    closedir($handle);
    return array_map(function ($entry) {
      if ($entry["is_dir"]) {
        $entry += [
          "children" => get_dir($entry["fullname"]),
        ];
      }
      return $entry;
    }, $entries);
  }
}
