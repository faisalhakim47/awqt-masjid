<?php


require_once __DIR__ . "/app.php";

$province = require_querystring("province");
$city = require_querystring("city");

$time_ids = [1,2,3,4,5];

foreach ($time_ids as $time_id) {
  $time_crons = execute_sql("
    SELECT cron_id
    FROM time_crons
    WHERE time_id = :time_id
  ", [
    ":time_id" => [$time_id, PDO::PARAM_INT],
  ]);
  execute_sql("DELETE FROM computed_crons");
  execute_sql("DELETE FROM time_crons WHERE time_id = :time_id", [
    ":time_id" => [$time_id, PDO::PARAM_INT],
  ]);
  foreach ($time_crons as $time_cron) {
    execute_sql("DELETE FROM crons WHERE id = :cron_id", [
      ":cron_id" => [$time_cron["cron_id"], PDO::PARAM_INT],
    ]);
  }
}

require_once __DIR__ . "/import-time/{$province}/{$city}.php";
