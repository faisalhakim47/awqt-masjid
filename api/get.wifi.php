<?php

require_once __DIR__ . "/app.php";

$ssid = "";
$passphrase = "";

foreach (explode("\n", file_get_contents("/etc/hostapd/hostapd.conf")) as $property)  {
  $property = explode("=", $property);
  if ($property[0] === "ssid") {
    $ssid = $property[1];
  }
  else if ($property[0] === "wpa_passphrase") {
    $passphrase = $property[1];
  }
};

send_json(200, [
  "ssid" => $ssid,
  "passphrase" => $passphrase,
]);
