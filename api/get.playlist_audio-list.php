<?php

require_once __DIR__ . "/app.php";

$id = (int) require_querystring("id");

$playlist = execute_sql("
  SELECT id, name, audio_md5s
  FROM playlists
  WHERE id = :id
", [
  ":id" => [$id, PDO::PARAM_INT],
])->fetch();

$audio_md5s = json_decode($playlist["audio_md5s"] ?: "[]", true);

$audios = array_map(function ($audio_md5) {
  return execute_sql("
    SELECT *
    FROM audios
    WHERE md5 = :md5
  ", [
    "md5" => [$audio_md5, PDO::PARAM_STR],
  ])->fetch();
}, $audio_md5s);

send_json(200, [
  "id" => $playlist["id"],
  "name" => $playlist["name"],
  "audios" => $audios,
]);
