<?php

require_once __DIR__ . "/app.php";

$command = require_querystring("command");
$result = file_get_contents("http://127.0.0.1:7777/" . $command);

if ($result) {
  send_json(200, [
    "ok" => true,
    "result" => $result,
  ]); 
}
else {
  send_json(500, [
    "error" => $result,
  ]);
}
