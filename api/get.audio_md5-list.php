<?php

require_once __DIR__ . "/app.php";

$audios = execute_sql("
  SELECT md5
  FROM audios
")->fetchAll();

send_json(200, array_map(function ($audio) {
  return $audio["md5"];
}, $audios));
