<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/../awqt/api/model/audio.php";

$audio_files = get_dir(__DIR__ . "/import-audios");

foreach ($audio_files as $audio_file) {
  ensure_audio($audio_file["name"], $audio_file["fullname"]);
}

send_json(200, $audio_files);
