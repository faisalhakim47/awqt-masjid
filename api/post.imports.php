<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/../awqt/api/model/audio.php";
require_once __DIR__ . "/../awqt/api/model/schedule.php";
require_once __DIR__ . "/../awqt/api/model/tag.php";

function masjid_imports($dir)
{
  $schedule_packs = get_dir($dir);

  $day_map = [
    "1 Ahad" => 1,
    "2 Senin" => 2,
    "3 Selasa" => 3,
    "4 Rabu" => 4,
    "5 Kamis" => 5,
    "6 Jum'at" => 6,
    "7 Sabtu" => 7,
  ];

  $time_map = [
    "1 Subuh" => 1,
    "2 Dzuhur" => 2,
    "3 Ashar" => 3,
    "4 Maghrib" => 4,
    "5 Isya" => 5,
  ];

  $results = [];

  foreach ($schedule_packs as $schedule_pack) {
    $pack_name = $schedule_pack["name"];
    $tag_name = ":{$pack_name}:";

    if (!$schedule_pack["is_dir"] || is_tag_exist($tag_name)) {
      continue;
    }

    usort($schedule_pack["children"], function ($a, $b) {
      return $a['name'] - $b['name'];
    });

    foreach ($schedule_pack["children"] as $day) {
      $day_id = $day_map[$day["name"]];
      if (!$day["is_dir"] || !is_int($day_id)) {
        continue;
      }
      foreach ($day["children"] as $time) {
        $time_id = $time_map[$time["name"]];
        if (!$time["is_dir"] || !is_int($time_id)) {
          continue;
        }
        $schedule = [
          "name" => ":{$tag_name}:{$day_id}:{$time_id}:",
          "tags" => [$tag_name],
          "tasks" => [],
          "include_times" => [
            ["time_id" => $time_id],
          ],
          "intersect_crons" => [[
            "months" => "*",
            "dates" => "*",
            "days" => "{$day_id}",
            "hours" => "*",
            "minutes" => "*",
            "seconds" => "*",
          ]],
          "exclude_crons" => [],
        ];

        usort($time["children"], function ($a, $b) {
          return $a['name'] - $b['name'];
        });

        $audios = [];
        foreach ($time["children"] as $key => $audio_file) {
          if ($audio_file["is_dir"]) {
            continue;
          }
          $audio = ensure_audio($audio_file["name"], $audio_file["fullname"]);
          array_push($audios, $audio["md5"]);
        }

        array_push($schedule["tasks"], [
          "name" => "AudioPlayUsingSpeaker",
          "payload" => json_encode([
            "md5s" => $audios,
          ]),
          "timing" => "before",
        ]);

        $result = create_schedule_pack($schedule);

        array_push($results, $result);
      }
    }
  }

  execute_sql("
    UPDATE tags
    SET is_active = 0
    WHERE name LIKE ':%:'
  ")->fetch();

  execute_sql("
    UPDATE tags
    SET is_active = 1
    WHERE name LIKE ':%:'
    LIMIT 1
  ")->fetch();

  return $results;
}

$result = masjid_imports(__DIR__ . "/import-schedule-pack");

send_json(200, $result);
