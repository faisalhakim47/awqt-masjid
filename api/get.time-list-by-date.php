<?php

require_once __DIR__ . "/app.php";

$months = require_querystring("months");
$dates = require_querystring("dates");

$result = execute_sql("
  SELECT
    times.id as time_id,
    times.name as time_name,
    crons.months as months,
    crons.dates as dates,
    crons.hours as hours,
    crons.minutes as minutes
  FROM times
  JOIN time_crons ON time_crons.time_id = times.id
  JOIN crons ON crons.id = time_crons.cron_id
  WHERE crons.months = :months AND crons.dates = :dates
", [
  ":months" => [$months, PDO::PARAM_STR],
  ":dates" => [$dates, PDO::PARAM_STR],
])->fetchAll();

send_json(200, $result);
