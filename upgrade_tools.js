const child_process = require('child_process');
const pm2 = require('pm2');
const isOnline = require('is-online');
const { log } = require('./awqt/awqt/tools/log.js');
const { build } = require('./build_system.js');

const HOME = '/home/' + child_process.execSync('whoami').slice(0, -1);
const AWQT_MASJID = HOME + '/workspace/awqt-masjid';

function stopApps() {
  return new Promise((resolve, reject) => {
    pm2.stop('awqt', (error) => {
      if (error) return reject(error);
      resolve();
    });
  });
}

function startApps() {
  return new Promise((resolve, reject) => {
    pm2.start('awqt', (error) => {
      if (error) return reject(error);
      resolve();
    });
  });
}

function restartUpdater() {
  setTimeout(() => {
    pm2.restart('awqt_updater', (error) => {
      if (error) {
        log('RestartUpdater:Error', error);
      }
    });
  }, 5000);
  return Promise.resolve({ ok: true });
}

function execute(command) {
  return new Promise((resolve, reject) => {
    child_process.exec(command, (error, stdout, stderr) => {
      if (error) reject({ error, stderr });
      else resolve(stdout);
    });
  });
}

function upgradeRepositories() {
  return isOnline()
    .then((online) => {
      if (!online) throw { msg: 'offline' };
    })
    .then(() => execute(`cd ${AWQT_MASJID} && git checkout master && git checkout . && git pull && npm install`))
    .then((awqtMasjid) =>
      execute(
        `cd ${AWQT_MASJID}/awqt && git checkout master && git checkout . && git pull && npm install`
      ).then((awqt) => {
        return {
          awqtMasjid,
          awqt,
        };
      })
    );
}

function reboot() {
  child_process.execSync('sudo reboot');
}

function upgradeDatabase() {
  return execute(`php ${AWQT_MASJID}/api/get.is_awqt_app.php`).then(
    (result) => {
      if (!(JSON.parse(result) || {}).ok) {
        throw new Error(result);
      }
      return { ok: true };
    }
  );
}

function upgradeAll() {
  return stopApps()
    .then(upgradeRepositories)
    .then(build)
    .then(upgradeDatabase)
    .then(startApps)
    .then(restartUpdater);
}

module.exports = {
  stopApps,
  startApps,
  upgradeRepositories,
  upgradeDatabase,
  upgradeAll,
  build,
  reboot,
  restartUpdater,
};
