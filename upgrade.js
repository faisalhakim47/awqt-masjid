const http = require('http');
const { log } = require('./awqt/awqt/tools/log.js');
const {
  startApps,
  stopApps,
  upgradeAll,
  upgradeDatabase,
  upgradeRepositories,
  build,
  reboot,
  restartUpdater,
} = require('./upgrade_tools.js');

http
  .createServer(async (request, response) => {
    response.setHeader('content-type', 'application/json');
    try {
      if (request.url === '/upgrade_all') {
        try {
          response.write(JSON.stringify(await upgradeAll()));
          response.end();
        } catch (error) {
          await startApps();
          response.statusCode = 500;
          response.write(JSON.stringify(error));
          response.end();
          log('Upgrade:UpgradeAll', error);
        }
      } else if (request.url === '/start_apps') {
        response.write(JSON.stringify(await startApps()));
        response.end();
      } else if (request.url === '/stop_apps') {
        response.write(JSON.stringify(await stopApps()));
        response.end();
      } else if (request.url === '/restart_apps') {
        await stopApps();
        await startApps();
        response.write(JSON.stringify({ ok: true }));
        response.end();
      } else if (request.url === '/restart_updater') {
        response.write(JSON.stringify(await restartUpdater()));
        response.end();
      } else if (request.url === '/update_database') {
        response.write(JSON.stringify(await upgradeDatabase()));
        response.end();
      } else if (request.url === '/update_repositories') {
        response.write(JSON.stringify(await upgradeRepositories()));
        response.end();
      } else if (request.url === '/build') {
        response.write(JSON.stringify({ result: (await build()).toString() }));
        response.end();
      } else if (request.url === '/reboot') {
        reboot();
        response.write(JSON.stringify({ ok: true }));
        response.end();
      } else {
        response.statusCode = 404;
        response.write(JSON.stringify({ url: response.url }));
        response.end();
      }
    } catch (error) {
      response.statusCode = 500;
      response.write(JSON.stringify(error));
      console.log(error);
      response.end();
    }
  })
  .listen(7777, '127.0.0.1');
